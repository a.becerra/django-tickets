from django.contrib import admin

# Register your models here.
from .models import tickets

class AdminTicketNew(admin.ModelAdmin):
    list_display = ["DocDate","TicketName","TicketType"]
    list_editable = ['TicketName','TicketType'] 
    class Meta:
        model = tickets

admin.site.register(tickets,AdminTicketNew)

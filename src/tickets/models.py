from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class tickets(models.Model):

    Bug     = 'bug'
    Ticket  = 'ticket'
    Other  = 'other'
    
    TicketType = (
        (Bug, 'Bug'),
        (Ticket, 'Ticket'),
        (Other, 'Otros'),
    )

    DocDate     = models.DateTimeField(auto_now=True, verbose_name='Fecha')
    TicketID    = models.AutoField(primary_key=True, verbose_name='ID') 
    TicketType  = models.CharField(max_length=10,choices=TicketType,default=Ticket, verbose_name='Tipo')
    TicketName  = models.CharField(max_length=100, verbose_name='Nombre')
    TicketDescription = models.CharField(max_length=600, verbose_name='Descripción')

    class Meta:
        verbose_name = ('Ticket')
        verbose_name_plural = ('Tickets')
        


    def __str__(self):
        return self.TicketName

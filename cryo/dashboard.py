from django.utils.translation import ugettext_lazy as _
from jet.dashboard import modules
from jet.dashboard.dashboard import Dashboard

from dashboard_modules_app import dashboard_modules


class CustomIndexDashboard(Dashboard):
    columns = 3

    def init_with_context(self, context):
        self.available_children.append(modules.LinkList)
        self.available_children.append(modules.AppList)
        self.available_children.append(modules.ModelList)
        self.available_children.append(modules.RecentActions)
        self.available_children.append(modules.Feed)
        self.available_children.append(dashboard_modules.RecentClients)
        self.available_children.append(dashboard_modules.Plot)
        self.available_children.append(dashboard_modules.Clock)
        self.available_children.append(dashboard_modules.Form)
        self.available_children.append(dashboard_modules.Maps)
        self.available_children.append(dashboard_modules.Inbox)
        self.available_children.append(dashboard_modules.MessageThreads)
        self.available_children.append(dashboard_modules.CreateMessage)
        self.available_children.append(dashboard_modules.Shell)
        self.available_children.append(dashboard_modules.SoftPhone)

        self.children.append(modules.LinkList(
            _('Support'),
            children=[
                {
                    'title': _('Django documentation'),
                    'url': 'http://docs.djangoproject.com/',
                    'external': True,
                },
                {
                    'title': _('Django "django-users" mailing list'),
                    'url': 'http://groups.google.com/group/django-users',
                    'external': True,
                },
                {
                    'title': _('Django irc channel'),
                    'url': 'irc://irc.freenode.net/django',
                    'external': True,
                },
            ],
            column=0,
            order=0
        ))

        self.children.append(modules.AppList(
            _('Applications'),
            exclude=('auth.*',),
            column=0,
            order=0
        ))

        self.children.append(modules.ModelList(
            _('Models'),
            exclude=('auth.*',),
            column=0,
            order=0
        ))

        self.children.append(modules.RecentActions(
            _('Recent Actions'),
            10,
            column=0,
            order=0
        ))

        self.children.append(modules.Feed(
            _('Latest Django News'),
            feed_url='http://www.djangoproject.com/rss/weblog/',
            limit=5,
            column=0,
            order=0
        ))
